<?
/**
 * Media Service, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 */

require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin.php");

$saleModulePermissions = $APPLICATION->GetGroupRight("medias.main");

if ($saleModulePermissions=="D")
	$APPLICATION->AuthForm(GetMessage("ACCESS_DENIED"));

require_once($_SERVER["DOCUMENT_ROOT"]."/local/modules/medias.main/include.php");

IncludeModuleLangFile(__FILE__);

ClearVars();
$errorMessage = "";

/*
 * Тут пишем оброботчики данніх из нашей формы, которую определим ниже.
 */

$aTabs = array(
		array("DIV" => "tab1", "TAB" => GetMessage("MEDIAS_MAIN_OPTION_TAB_1"), "ICON" => "medias.main", "TITLE" => GetMessage("MEDIAS_MAIN_OPTION_TITLE_TAB_MAIN"))
	);

$tabControl = new CAdminTabControl("tabControl", $aTabs);
$tabControl->Begin();
?>
<?
	/*
	 * Тут пишем то, что будет общим для всех вкладок на странице. Например шапку формі.
	 */
?>
<?
$tabControl->BeginNextTab();
?>

<?
	/*
	 * Тут пишем то, что будет отображаться в первой вкладке.
	 */
?>

<?
$tabControl->EndTab();
?>

<?
$tabControl->Buttons(
		array(
				"disabled" => ($saleModulePermissions < "U"),
				"back_url" => "/bitrix/admin/sale_account_admin.php?lang=".LANG.GetFilterParams("filter_")
			)
	);
?>

<?
$tabControl->End();
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>