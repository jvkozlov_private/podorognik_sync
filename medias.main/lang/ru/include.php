<?
/**
 * Ignis Fatum, LTD
 *
 * @author Vadim Podovalov <v.podovalov@ignisfatum.ltd>
 * @copyright Copyright 2017 Ignis Fatum, LTD. All rights reserved.
 */

$MESS["MODULE_IGNIS_MAIN_INCLUDE_TITLE"] = "Общие настройки";
$MESS["MODULE_IGNIS_MENU_LOT_HISTORY"] = "Торги за лотами";
$MESS["MODULE_IGNIS_MENU_LOT_PREOFFER"] = "Заявки на аукціон";
?>