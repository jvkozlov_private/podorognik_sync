<?
/**
*  Media Sevice, LLC
*
*@author Yurii Kozlov <y.kozlov08@gmail.com>
*/

$MESS["MODULE_MEDIAS_MAIN_OPTION_PRE"] = "function pre()";
$MESS["MODULE_MEDIAS_MAIN_OPTION_WRITE"] = "function write()";
$MESS["MODULE_MEDIAS_MAIN_OPTION_ARCONSOLE"] = "function ArConsole()";
$MESS["MODULE_MEDIAS_MAIN_OPTION_LOCAL_GROUP_ID"] = "Ідентифікатор локальної групи синхронізації завдань (число)";
$MESS["MODULE_MEDIAS_MAIN_OPTION_REMOTE_GROUP_ID"] = "Ідентифікатор віддаленої групи синхронізації завдань (число)";

$MESS["MODULE_MEDIAS_MAIN_OPTION_DEV_OPTIONS"] = "Dev functions";
$MESS["MODULE_MEDIAS_MAIN_OPTION_INTEGRATION_OPTIONS"] = "Параметри інтграції";

$MESS["MODULE_MEDIAS_MAIN_OPTION_DATA_TYPE_ERROR"] = "Введено неправильний формат данних";


?>