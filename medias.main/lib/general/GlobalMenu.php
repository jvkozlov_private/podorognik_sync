<?
/**
 * @author Yurii Kozlov <y.kozlov08@gmail.com> Media Service LLC
 */
namespace Medias\Main\General;

use Bitrix\Main\Loader;

/**
 *
 * @author Yurii Kozlov
 *        
 */
class GlobalMenu
{

    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        global $USER;
        if (! $USER->IsAdmin())
            return;
       
        
        $arItems[] = array(
            "text" => 'Звіт задач',
            "title" => 'Звіт задач',
            "items_id" => "menu_task_report",
            "url" => "medias_task_stages_log.php?lang=" . LANG,
           
        );
        
        $aGlobalMenu["global_menu_medias"] = array(
            "menu_id" => "Medias",
            "text" => "Medias",
            "title" => "Medias",
            "url" => "medias_options.php?lang=" . LANG,
            "sort" => "200",
            "items_id" => "global_menu_medias",
            "help_section" => "medias",
            "items" => $arItems,
            "more_url" => array(),
        );
    }
}