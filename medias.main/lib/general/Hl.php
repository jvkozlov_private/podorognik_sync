<?
/**
 * work only with allready created HL
 * @author Yurii Kozlov <y.kozlov08@gmail.com> Media Service LLC
 */
/**
 * *
 * map
 * __construct
 * setHlParams
 * getHlDataById
 * getByElementByFieldValue
 * addElementToHl
 */
namespace Medias\Main\General;

use Bitrix\Main\Loader;

/**
 *
 * @author yura
 *        
 */
class Hl
{

    protected $hlId;

    protected $hlName;

    protected $hlTableName;

    /**
     * *
     *
     * @param string $tableName
     * @throws \Bitrix\Main\ArgumentNullException
     * @return boolean|boolean|\Bitrix\Main\ArgumentException
     */
    function __construct($tableName = '')
    {
        if (! Loader::includeModule('highloadblock'))
            return false;

        if (strlen($tableName) > 1) {

            return $this->setHlParams($tableName);
        } else {
            throw new \Bitrix\Main\ArgumentNullException($tableName);
        }
    }

    /**
     * *
     *
     * @param string $tableName
     * @throws \Bitrix\Main\ArgumentNullException
     * @return boolean|\Bitrix\Main\ArgumentException
     */
    private function setHlParams($tableName = '')
    {
        if (strlen($tableName) > 1) {
            $dbRes = \Bitrix\Highloadblock\HighloadBlockTable::getList(array(
                'filter' => array(
                    'TABLE_NAME' => $tableName
                )
            ));

            if ($hlData = $dbRes->fetch()) {
                $this->hlId = $hlData['ID'];
                $this->hlName = $hlData['NAME'];
                $this->hlTableName = $hlData['TABLE_NAME'];

                return true;
            } else {

                return new \Bitrix\Main\ArgumentException('table is empty', $tableName);
            }
        } else {
            throw new \Bitrix\Main\ArgumentNullException($tableName);
        }
    }

    public function getId()
    {
        return $this->hlId;
    }

    /**
     *
     * @param number $hlId
     * @return array
     */
    protected function getHlDataById($hlId = 0)
    {
        if ($hlId == 0)
            $hlId = $this->hlId;

        $arResult = array();

        $hlBlockData = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlId);

        if ($hlBlock = $hlBlockData->fetch()) {

            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
            $entityDataClass = $entity->getDataClass();

            $dbData = $entityDataClass::getList(array(
                "select" => array(
                    "*"
                )
            ));

            while ($arData = $dbData->Fetch()) {
                $arResult[] = $arData;
            }
        }

        return $arResult;
    }

    /**
     *
     * @param string $field
     * @param array  $value
     * @throws \Bitrix\Main\ArgumentNullException
     * @return array
     */
    protected function getByElementByFieldValue($field = '', $value = array())
    {
        if (strlen(trim($field)) > 1 && ! empty($value)) {

            $arResult = array();

            if (is_string($value))
                $value = trim($value);

            $hlBlockData = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlId);

            if ($hlBlock = $hlBlockData->fetch()) {

                $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
                $entityDataClass = $entity->getDataClass();

                $dbData = $entityDataClass::getList(array(
                    "select" => array(
                        "*"
                    ),
                    "filter" => array(
                        trim($field) => $value
                    )
                ));

                while ($arData = $dbData->Fetch()) {
                    $arResult[] = $arData;
                }
            }

            return $arResult;
        } else {

            $arResult = array();

            $hlBlockData = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlId);

            if ($hlBlock = $hlBlockData->fetch()) {

                $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
                $entityDataClass = $entity->getDataClass();

                $dbData = $entityDataClass::getList(array(
                    "select" => array(
                        "*"
                    ),
                    "filter" => array()
                ));

                while ($arData = $dbData->Fetch()) {
                    $arResult[] = $arData;
                }
            }

            return $arResult;
        }
    }

    /**
     * *
     * add or update data by fireld => value
     *
     * @param array $arParams
     * @throws \Bitrix\Main\InvalidOperationException
     * @return boolean
     */
    public function addElementToHl($arParams = array(), $keyParam = array())
    {
        $hlBlockData = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlId);

        $arResult = array();

        if ($hlBlock = $hlBlockData->fetch()) {

            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
            $entityDataClass = $entity->getDataClass();

            $dbData = $entityDataClass::getList(array(
                "select" => array(
                    "*"
                ),
                "filter" => $keyParam
            ));

            while ($arData = $dbData->Fetch()) {
                $arResult = $arData;
            }
            
            if (! $arResult) {
                
                $result = $entityDataClass::add($arParams);
                
                if ($result->isSuccess())
                    return $result->getId();
                else
                    throw new \Bitrix\Main\InvalidOperationException('add error');
            } else {
                
                
                foreach($keyParam as $key => $val){
                    
                    unset($arParams[$key]);
                }

                $result = $entityDataClass::update($arResult['ID'], $arParams);
               
                if ($result->isSuccess())
                    return true;
                else
                    throw new \Bitrix\Main\InvalidOperationException('update error');
                ;
            }
        }
    }

    /**
     * *
     *
     * @return array
     */
    public function getFields()
    {
        $arRes = array();
        $arFields = array();
        $arNotSupFields = array(
            'ID',
            /*'UF_ID',
            'UF_DELETED'*/
        );

        $hlBlockData = \Bitrix\Highloadblock\HighloadBlockTable::getById($this->hlId);

        if ($hlBlock = $hlBlockData->fetch()) {

            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
            $arDbFields = $entity->getFields();
        }

        foreach ($arDbFields as $fieldName => $obData) {
            if (! in_array($fieldName, $arNotSupFields)) {

                $rsFiled = \CUserTypeEntity::GetList(array(), array(
                    'ENTITY_ID' => 'HLBLOCK_' . $this->hlId,
                    'FIELD_NAME' => $fieldName
                ));

                while ($ar = $rsFiled->GetNext(true, false)) {
                    $arFields[$fieldName]['id'] = $ar['ID'];
                    $arFields[$fieldName]['type'] = $ar['USER_TYPE_ID'];
                }
            }
        }

        foreach ($arFields as $fieldName => $arData) {
            if ($arData['type'] == 'enumeration') {

                $rsEnumVals = \CUserFieldEnum::GetList(array(), array(
                    "USER_FIELD_ID" => $arData['id']
                ));
                while ($arEnumVals = $rsEnumVals->GetNext())
                    $arFields[$fieldName]['values'][$arEnumVals['ID']] = $arEnumVals["VALUE"];
            }

            $arFields[$fieldName]['labels'] = $this->getUserFieldLangs($arData['id']);
        }

        $arRes = $arFields;

        unset($arFields);
        return $arRes;
    }

    /**
     * *
     *
     * @param number $userFieldId
     * @return array
     */
    public function getUserFieldLangs($userFieldId = 0)
    {
        global $DB;
        $arLabels = array(
            "EDIT_FORM_LABEL",
            "LIST_COLUMN_LABEL",
            "LIST_FILTER_LABEL",
            "ERROR_MESSAGE",
            "HELP_MESSAGE"
        );
        $arRes = array();

        $rs = $DB->Query("SELECT * FROM b_user_field_lang WHERE USER_FIELD_ID = " . intval($userFieldId), false);
        while ($ar = $rs->Fetch()) {
            foreach ($arLabels as $label)
                $arRes[$label][$ar["LANGUAGE_ID"]] = $ar[$label];
        }

        return $arRes;
    }

    /**
     *
     * @return string
     */
    public function generateGuid()
    {
        if (function_exists('com_create_guid')) {

            return com_create_guid();
        } else {
            mt_srand((double) microtime() * 10000); // optional for php 4.2.0 and up.
            $charid = strtolower(md5(uniqid(rand(), true)));
            $hyphen = chr(45); // "-"
            $uuid = substr($charid, 0, 8) . $hyphen . substr($charid, 8, 4) . $hyphen . substr($charid, 12, 4) . $hyphen . substr($charid, 16, 4) . $hyphen . substr($charid, 20, 12);

            return $uuid;
        }
    }

    public function getList($arFilter = array(),$arSelect = array('*'))
    {
        $hlId = $this->hlId;

        $arResult = array();

        $hlBlockData = \Bitrix\Highloadblock\HighloadBlockTable::getById($hlId);

        if ($hlBlock = $hlBlockData->fetch()) {

            $entity = \Bitrix\Highloadblock\HighloadBlockTable::compileEntity($hlBlock);
            $entityDataClass = $entity->getDataClass();

            $dbData = $entityDataClass::getList(array(
                "select" => $arSelect,
                "filter" => $arFilter
            ));

            while ($arData = $dbData->Fetch()) {
                $arResult[] = $arData;
            }
        }

        return $arResult;
    }

    
}

?>