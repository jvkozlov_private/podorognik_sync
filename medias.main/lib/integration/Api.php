<?php
namespace Medias\Main\Integration;

use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Rest\RestException;

class Api extends \IRestService
{

    public static function registerInterface()
    {
        return array(
            'medias.main' => array(
                'medias.tasks.comments.register.list' => array(
                    Api::class,
                    'getRegisterList'
                ),
                'medias.tasks.comments.list' => array(
                    Api::class,
                    'getCommentsList'
                ),
                'medias.tasks.comments.add' => array(
                    Api::class,
                    'add'
                ),
                'medias.tasks.comments.register.add' => array(
                    Api::class,
                    'addRegister'
                ),
                'medias.tasks.comments.update' => array(
                    Api::class,
                    'update'
                ),
                'medias.main.uploadfile' => array(
                    Api::class,
                    'uploadFile'
                ),
                'medias.main.user.register.check' => array(
                    Api::class,
                    'checkUserInRegister'
                ),
                'medias.main.attach.register.add' => array(
                    Api::class,
                    'setAttechedObjectIdToRegister'
                ),
                'medias.main.attach.get.byobject' => array(
                    Api::class,
                    'getDiskAttachedObjecByObjectId'
                ),
                'medias.main.task.reports.podorognik.get' => array(
                    Api::class,
                    'getTaskReportsPodorognik'
                )
            )
        );
    }

    public function getDescription(): array
    {
        $scopes = [];

        $scopes['medias'] = [];

        return $scopes;
    }

    /**
     * *
     *
     * @param [] $params
     *            ['filter', 'select']
     * @throws RestException
     * @return mixed|string
     */
    public static function getRegisterList($params)
    {
        if (! Loader::includeModule('medias.main')) {
            throw new RestException('medias.main', 'no_module');
        }

        if (! Loader::includeModule('highloadblock')) {
            throw new RestException('highloadblock', 'no_module');
        }

        try {

            $arFilter = [];
            $arSelect = [];
            $arResult = [];

            if (is_array($params['filter'])) {
                $arFilter = $params['filter'];
            }

            if (is_array($params['select'])) {
                $arSelect = $params['select'];
            }

            $obHl = new \Medias\Main\General\Hl('m_task_comment_register');

            $arResult = $obHl->getList($arFilter, $arSelect);

            return \Bitrix\Main\Web\Json::encode($arResult);
        } catch (\Exception $e) {
            throw new RestException($e->getMessage());
        }
    }

    /**
     * *
     *
     * @param [] $params
     *            ['filter','select','order']
     * @throws RestException
     * @return array
     */
    public static function getCommentsList($params)
    {
        if (! Loader::includeModule('medias.main')) {
            throw new RestException('medias.main', 'no_module');
        }

        try {

            $arFilter = [];
            $arSelect = [];
            $arOrder = [];
            $arResult = [];

            if (is_array($params['filter'])) {
                $arFilter = $params['filter'];
            }

            if (is_array($params['select'])) {
                $arSelect = $params['select'];
            }

            if (is_array($params['order'])) {
                $arOrder['order'] = $params['order'];
            }

            $arSelect[] = 'UF_REMOTE_COMMENT';

            $dbRes = \CForumMessage::GetList($arOrder, $arFilter, false, 0, [
                "SELECT" => $arSelect
            ]);

            $arResult = [];

            while ($a = $dbRes->GetNext(true, false)) {
                $arResult[] = $a;
            }

            return $arResult;
        } catch (\Exception $e) {
            throw new RestException($e->getMessage());
        }
    }

    /**
     * *
     *
     * @param [] $params
     *            ['fields']
     * @throws RestException
     * @return number
     */
    public static function add($params)
    {
        if (! Loader::includeModule('medias.main')) {
            throw new RestException('medias.main', 'no_module');
        }

        if (! Loader::includeModule('forum')) {
            throw new RestException('forum', 'no_module');
        }

        if (! Loader::includeModule('tasks')) {
            throw new RestException('tasks', 'no_module');
        }
        
        if (! Loader::includeModule('im')) {
            throw new RestException('im', 'no_module');
        }

        try{

            $arFields = $params['fields'];

            $commentRemoteId = $arFields['UF_COMMENT_ID'];
            $taskId = $arFields['TASK_ID'];

            unset($arFields['TASK_ID']);
            unset($arFields['UF_COMMENT_ID']);

            // $arFields['UF_REMOTE_COMMENT'] = 'Y';
            $arFields['AUX'] = 'Y';

            // по id автора отримати імя атора

            $arTask = new \CTaskItem($taskId,  $arFields['AUTHOR_ID']);

            $responsibleId = $arTask['RESPONSIBLE_ID'];

            $commentResult = \Bitrix\Tasks\Integration\Forum\Task\Comment::add($taskId, $arFields);

            $arCommentResult = $commentResult->getData();

            $obHl = new \Medias\Main\General\Hl('m_task_comment_register');

            $obHl->addElementToHl([
                'UF_COMMENT_ID' => $arCommentResult['ID']
            ], [
                'UF_COMMENT_REMOTE_ID' => $commentRemoteId
            ]);

            $taskLinkString = '[url = https://bitrix24.podorozhnyk.net/company/personal/user/' . $responsibleId . '/tasks/task/view/' . $taskId . '/]' . $taskId . '[/url]';

            $fields = array(
                "TO_USER_ID" => $responsibleId,
                "FROM_USER_ID" => $arFields['AUTHOR_ID'],
                "NOTIFY_TYPE" => IM_NOTIFY_FROM,
                "NOTIFY_MODULE" => "sender",
                // "NOTIFY_EVENT" => $imNotifyEvent,
                // "NOTIFY_TAG" => $notifyTag,
                "NOTIFY_MESSAGE" => 'Додано коментар до задачі №' . $taskLinkString
            );

            \CIMNotify::Add($fields);

            return $arCommentResult['ID'];
        }catch (\Exception $e) {

            return ['error' => true, 'description' => $e->getMessage()];
        }
    }

    /**
     * *
     *
     * @param array $params
     */
    public static function update(array $params)
    {
        if (! Loader::includeModule('medias.main')) {
            throw new RestException('medias.main', 'no_module');
        }

        if (! Loader::includeModule('forum')) {
            throw new RestException('forum', 'no_module');
        }

        $arFields = $params['FIELDS'];

        $commentRemoteId = $arFields['UF_COMMENT_ID'];
        
        //$taskId = $arFields['TASK_ID'];

        unset($arFields['TASK_ID']);
        unset($arFields['UF_COMMENT_ID']);

        $arFields['AUX'] = 'Y';

        $res = \CForumMessage::Update($commentRemoteId, $arFields);
        
        return $res;
    }

    /**
     * **
     *
     * @param [] $params
     *            ['fields']
     * @throws RestException
     * @return boolean
     */
    public static function addRegister($params)
    {
        if (! Loader::includeModule('medias.main')) {
            throw new RestException('medias.main', 'no_module');
        }

        if (! Loader::includeModule('highloadblock')) {
            throw new RestException('highloadblock', 'no_module');
        }

        try {

            $arFields = $params['fields'];

            $obHl = new \Medias\Main\General\Hl('m_task_comment_register');

            $resAdd = $obHl->addElementToHl($arFields, [
                'UF_COMMENT_ID' => $arFields['UF_COMMENT_ID'],
                'UF_COMMENT_REMOTE_ID' => $arFields['UF_COMMENT_REMOTE_ID']
            ]);

            return $resAdd;
        } catch (\Exception $e) {
            throw new RestException($e->getMessage());
        }
    }

    public static function checkUserInRegister($params)
    {
        if ($params['userId'] > 0) {

            if (! Loader::includeModule('medias.main')) {
                throw new RestException('medias.main', 'no_module');
            }

            if (! Loader::includeModule('highloadblock')) {
                throw new RestException('highloadblock', 'no_module');
            }

            $userId = $params['userId'];

            $obHl = new \Medias\Main\General\Hl('m_remote_user');

            $resDb = $obHl->getList([
                'UF_REMOTE_USER_ID' => $userId
            ]);

            if ($resDb[0]['UF_USER_ID'] > 0)
                return true;
            else
                return false;
        } else {

            return [
                'error' => true,
                'mess' => 'Bad params'
            ];
        }
    }

    /**
     * *
     * [file]
     * [moduleId]
     * [entityId]
     * [userId]
     *
     * @param array $params
     * @throws RestException
     * @return \Bitrix\Disk\File|NULL|unknown|boolean[]|string[]
     */
    public static function uploadFile(array $params)
    {
        if (/*$params['entityId'] > 0 &&*/ is_array($params['file']) > 0) {

            if (! Loader::includeModule('medias.main')) {
                throw new RestException('medias.main', 'no_module');
            }
            if (! Loader::includeModule('disk')) {
                throw new RestException('disk', 'no_module');
            }

            if (! isset($params['userId']))
                $userId = \Bitrix\Disk\SystemUser::SYSTEM_USER_ID;
            else
                $userId = $params['userId'];

            $connectorClass = [
                'forum' => 'Bitrix\Disk\Uf\ForumMessageConnector',
                'tasks' => 'Bitrix\Tasks\Integration\Disk\Connector\Task'
            ];

            //$newFileSrc = $params['file']['FULL_PATH'];
            file_put_contents($_SERVER['DOCUMENT_ROOT'].'/upload/'.$params['file']['NAME'],base64_decode($params['file']['DATA']));
            $newFileSrc = $_SERVER['DOCUMENT_ROOT'].'/upload/'.$params['file']['NAME'];

            $driver = \Bitrix\Disk\Driver::getInstance();

            $storage = $driver->getStorageByGroupId(\Medias\Main\Integration\Podorognik::LOCAL_GROUP_ID);

            // $storage = $driver->getStorageByUserId($userId);

            if ($storage) {

                $folderRoot = $storage->getRootObject();

                $folderUpldFiles = $folderRoot->getChild(array(
                    '=NAME' => 'upload',
                    'TYPE' => \Bitrix\Disk\Internals\FolderTable::TYPE_FOLDER
                ));

                if (! $folderUpldFiles) {

                    $folderUpldFiles = $folderRoot->getChild(array(
                        '=NAME' => 'Upload',
                        'TYPE' => \Bitrix\Disk\Internals\FolderTable::TYPE_FOLDER
                    ));
                }

                if ($folderUpldFiles) {
                    
                    $fileArray = \CFile::MakeFileArray($newFileSrc);
                    
                    $arFile = $folderUpldFiles->uploadFile($fileArray, array(
                        'CREATED_BY' => intval($userId),
                        'NAME' => $params['file']['NAME']
                    ));
                 
                    unlink($newFileSrc);
                    if ($arFile != NULL) {
                        $arRegister = [];

                        $arRegister['UF_LOCAL_FILE_ID'] = $arFile->getFileId();
                        $arRegister['UF_REMOTE_FILE_ID'] = $params['file']['ID'];

                        $obList = new \Medias\Main\General\Hl('m_task_file_register');

                        $obList->addElementToHl($arRegister, [
                            'UF_REMOTE_FILE_ID' => $params['file']['ID']
                        ]);

                        $errorCollection = new \Bitrix\Disk\Internals\Error\ErrorCollection();

                        $attachedModel = \Bitrix\Disk\AttachedObject::add(array(
                            'MODULE_ID' => $params['moduleId'],
                            'OBJECT_ID' => $arFile->getId(),
                            'ENTITY_ID' => $params['entityId'],
                            'ENTITY_TYPE' => $connectorClass[$params['moduleId']],
                            'IS_EDITABLE' => 1,
                            'ALLOW_EDIT' => 1,
                            'CREATED_BY' => $userId
                        ), $errorCollection);

                        return [
                            'attachedModelId' => $attachedModel->getId(),
                            'objectId' => $arFile->getId()
                        ];
                    } else {
                        $obList = new \Medias\Main\General\Hl('m_task_file_register');

                        $arRes = $obList->getList([
                            'UF_REMOTE_FILE_ID' => $params['file']['ID']
                        ]);

                        if (! empty($arRes)) {
                            $resObjects = \Bitrix\Disk\Internals\ObjectTable::getList([
                                'select' => [
                                    'NAME',
                                    'FILE_ID',
                                    'ID'
                                ],
                                'filter' => [
                                    '=FILE_ID' => $arRes[0]['UF_LOCAL_FILE_ID']
                                ]
                            ]);

                            $arObject = $resObjects->fetch();

                            $errorCollection = new \Bitrix\Disk\Internals\Error\ErrorCollection();

                            $attachedModel = \Bitrix\Disk\AttachedObject::add(array(
                                'MODULE_ID' => $params['moduleId'],
                                'OBJECT_ID' => $arObject['ID'],
                                'ENTITY_ID' => $params['entityId'],
                                'ENTITY_TYPE' => $connectorClass[$params['moduleId']],
                                'IS_EDITABLE' => 1,
                                'ALLOW_EDIT' => 0,
                                'CREATED_BY' => $userId
                            ), $errorCollection);

                            return [
                                'attachedModelId' => $attachedModel->getId(),
                                'objectId' => $arObject['ID']
                            ];
                        }else{
                            return [
                                'error' => true,
                                'mess' => 'no file'
                            ];
                        }
                    }
                } else {

                    return [
                        'error' => true,
                        'mess' => 'no folder "upload"'
                    ];
                }

             
            } else {

                return [
                    'error' => true,
                    'mess' => 'no storage for user'
                ];
            }
        } else {

            return [
                'error' => true,
                'mess' => 'Bad params'
            ];
        }
    }

    /**
     * *
     * $params[attacheId]
     * $params[remoteFileId]
     *
     * @param array $params
     */
    public function setAttechedObjectIdToRegister(array $params)
    {
        if (isset($params['attacheId']) && isset($params['remoteFileId'])) {

            if (! Loader::includeModule('medias.main')) {
                throw new RestException('medias.main', 'no_module');
            }

            $obList = new \Medias\Main\General\Hl('m_task_file_register');

            $obList->addElementToHl([
                'UF_LOCAL_FILE_ID' => $params['attacheId'],
                'UF_REMOTE_FILE_ID' => $params['remoteFileId']
            ], [
                'UF_LOCAL_FILE_ID' => $params['attacheId']
            ]);
        }
    }

    public function getDiskAttachedObjecByObjectId(array $params)
    {
        if (isset($params['objectId'])) {

            if (! Loader::includeModule('medias.main')) {
                throw new RestException('medias.main', 'no_module');
            }

            $arRes = \Bitrix\Disk\Internals\AttachedObjectTable::getList([
                'select' => [
                    'ID'
                ],
                'filter' => [
                    '=ID' => $params['objectId']
                ]
            ])->fetch();

            return $arRes;
        } else {
            return [
                'error' => true,
                'mess' => 'Bad params'
            ];
        }
    }

    public function getTaskReportsPodorognik(array $arParams)
    {
        \Bitrix\Main\Loader::includeModule('tasks');

        $taskGroupId = 273;
        
        $arResult = [];

        $arFilter = [];

        $arTaskStages = \Bitrix\Tasks\Kanban\StagesTable::getList([
            'filter' => [
                'ENTITY_TYPE' => 'G',
                'ENTITY_ID' => $taskGroupId
            ]
        ])->fetchAll();

        $arResultStages = [];

        foreach ($arTaskStages as $arStage) {

            $arResultStages[$arStage['ID']] = $arStage['TITLE'];
        }

        $arUsersFields = [
            'RESPONSIBLE_ID',
            'CREATED_BY',
            'CREATED_BY_ID',
            'MODIFY_BY_ID'
        ];
        
        $arDateFields = [
            'CHANGED_DATE',
            'CREATED_DATE',
            'DATE_CREATE',
            'DATE_MODIFY',
        ];

        if (isset($arParams['from']))
            $arFilter['>=CHANGED_DATE'] = $arParams['from'];

        if (isset($arParams['to']))
            $arFilter['<CHANGED_DATE'] = $arParams['to'];

        $arTableResult = \Medias\Main\Tasks\ReportTable::getList([
            'filter' => $arFilter,
            'order' => ['CHANGED_DATE' => 'asc'],
        ])->fetchAll();

        $arTableMap = \Medias\Main\Tasks\ReportTable::getMap();

        $arHeaders = array_keys($arTableMap);

        // $totalRows = count($arTableResult);

        if (\Bitrix\Main\Loader::includeModule('pai.phpoffice')) {

            $application = \Bitrix\Main\Application::getInstance();
            
            $docRoot = \Bitrix\Main\Application::getDocumentRoot();

            $dir = $docRoot . '/upload/tmp_ms';

            if (! is_dir($dir))
                mkdir($dir);

            $fileLink = $dir . '/task_report.xlsx';

            $obServer =  new \Bitrix\Main\Server($_SERVER);
            
            $downloadLink = 'https://'. $obServer->getServerName() . '/upload/tmp_ms/task_report.xlsx';
            
            $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet(); // нова книга

            $sheet = $spreadsheet->getActiveSheet();

            $colIndex = 0;

            foreach ($arHeaders as $header) {

                $sheet->setCellValueByColumnAndRow($colIndex, 1, $header);

                $colIndex ++;
            }

            $rowIndex = 2;

            foreach ($arTableResult as $tableRow) {

                foreach ($arHeaders as $colIndex => $header) {

                    // $strIndex = \PhpOffice\PhpSpreadsheet\Cell::stringFromColumnIndex($colIndex);

                    $val = $tableRow[$header];

                    if ($header == 'STAGE_ID')
                        $val = $arResultStages[$tableRow[$header]];

                    if (in_array($header, $arUsersFields)) {

                        $arUser = \Bitrix\Main\UserTable::getList([
                            'filter' => [
                                'ID' => intval($tableRow[$header])
                            ],
                            'select' => [
                                'NAME',
                                'LAST_NAME'
                            ]
                        ])->fetch();

                        $val = implode(' ', $arUser);
                    }

                    $sheet->setCellValueByColumnAndRow($colIndex, $rowIndex, $val);
                }

                $rowIndex ++;
            }

            $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
            $writer->save($fileLink);

            $arResult['fileName'] = 'task_report.xlsx';
           // $arResult['data'] = base64_encode(file_get_contents($fileLink));

            // unlink($fileLink);
        }

        $arResult['headers'] = $arHeaders;
        $arResult['downLoadLink'] = $downloadLink;

        $arResultRows = [];

        $rowIndex = 1;

        foreach ($arTableResult as $arRow) {

            foreach ($arHeaders as $colIndex => $header) {

                $val = $arRow[$header];

                if ($header == 'STAGE_ID')
                    $val = $arResultStages[$arRow[$header]];

                if (in_array($header, $arUsersFields)) {

                    $arUser = \Bitrix\Main\UserTable::getList([
                        'filter' => [
                            'ID' => intval($arRow[$header])
                        ],
                        'select' => [
                            'NAME',
                            'LAST_NAME'
                        ]
                    ])->fetch();

                    $val = implode(' ', $arUser);
                }
                
                if(in_array($header, $arDateFields))
                    $val = $arRow[$header]->toString();                

                $arResultRows[$rowIndex][$header] = $val;
            }

            $rowIndex ++;
        }

        $arResult['rows'] = $arResultRows;

        return $arResult;
    }
}

?>