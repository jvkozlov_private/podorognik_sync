<?
/**
 *  Media Sevice, LLC
 *
 *@author Yurii Kozlov <y.kozlov08@gmail.com>
 */
namespace Medias\Main\Integration;

use Bitrix\Main\Config\Option;

class Podorognik
{

    public static $TECH_USER = 1;

    private static $restUrl = 'https://portal.medias.com.ua/';

    private static $restFileSendMedias = '1144/c9jn7u59flp49out/';

    private static $hookTaskAddCode = '1144/c9jn7u59flp49out/';

    private static $hookTaskUpdateCode = '1144/c9jn7u59flp49out/';

    private static $hookTaskAddFileCode = '1144/c9jn7u59flp49out/';

    private static $hookTaskAddCommentItemCode = '1144/c9jn7u59flp49out/';

    private static $hookTaskUpdateCommentItemCode = '1144/c9jn7u59flp49out/';

    private static $hookUserGetCode = '1144/c9jn7u59flp49out/';

    
    /*  private static $restUrl = 'https://ms2.medias.com.ua/';
     
      private static $hookTaskAddCode = '1/k2ypp86dw2idnjw3/';
     
      private static $hookTaskUpdateCode = '1/k2ypp86dw2idnjw3/';
     
      private static $hookTaskAddFileCode = '1/k2ypp86dw2idnjw3/';
     
      private static $hookTaskAddCommentItemCode = '1/k2ypp86dw2idnjw3/';
     
      private static $hookTaskUpdateCommentItemCode = '1/k2ypp86dw2idnjw3/';
     
      private static $hookUserGetCode = '1/k2ypp86dw2idnjw3/';
     
      private static $restFileSendMedias = '1/k2ypp86dw2idnjw3/';*/
     

    // test extranet 285 / 273
    // const REMOTE_GROUP_ID = 633;
    const REMOTE_GROUP_ID = 273;

    // podorognik 633 / 8
    // const LOCAL_GROUP_ID = 273;
    const LOCAL_GROUP_ID = 633;

    private static $timezone = 2;

    // podorognik 11
    private static $taskCommentForumId = 8;

    public $arFieldsToBeChanged = [];

    function __construct()
    {
        if (! \Bitrix\Main\Loader::includeModule('main')) {

            return false;
        }
        if (! \Bitrix\Main\Loader::includeModule('tasks')) {

            return false;
        }
        if (! \Bitrix\Main\Loader::includeModule('rest')) {

            return false;
        }
        if (! \Bitrix\Main\Loader::includeModule('disk')) {

            return false;
        }
        if (! \Bitrix\Main\Loader::includeModule('im')) {

            return false;
        }
    }

    public function start(){
        
        \Bitrix\Main\Loader::includeModule('medias.main');
    }

    /**
     * *
     *
     * @param int $id
     * @param array $arTask
     */
    public function OnTaskAdd(int $id, array &$arTask)
    {
        if ($arTask['GROUP_ID'] == self::LOCAL_GROUP_ID) {
            if (! $arTask['UF_MEDIAS_TASK_REMOTE_ID']) {
                \Bitrix\Main\Loader::includeModule('disk');
                \Bitrix\Main\Loader::includeModule('tasks');
                \Bitrix\Main\Loader::includeModule('im');
                // add only one group
                $arParams = [];

                $arParams['url'] = self::$restUrl . 'rest/' . self::$hookTaskAddCode . 'task.item.add.json';
                $arParams['followlocation'] = true;
                $arParams['ssl_ignore'] = true;

                $arTask['GROUP_ID'] = self::REMOTE_GROUP_ID;

                // $arTask['CREATED_BY'] = self::getRemoteUserId($arTask['CREATED_BY']);
                // $arTask['RESPONSIBLE_ID'] = self::getRemoteUserId($arTask['RESPONSIBLE_ID']);
                // $arTask['CHANGED_BY'] = self::getRemoteUserId($arTask['CHANGED_BY']);
                // $arTask['STATUS_CHANGED_BY'] = self::getRemoteUserId($arTask['STATUS_CHANGED_BY']);
                $arAuditors = [];
                $arAcomplices = [];

                foreach ($arTask['AUDITORS'] as $auditor) {

                    if (intval($auditor) > 0)
                        $arAuditors[] = self::getRemoteUserId($auditor);
                }

                foreach ($arTask['ACCOMPLICES'] as $accomplicer) {

                    if (intval($accomplicer) > 0)
                        $arAcomplices[] = self::getRemoteUserId($accomplicer);
                }

                $description = self::checkText($arTask['DESCRIPTION']);

                // $description = self::checkTextFile($description, $arTask['CREATED_BY']);

                if (isset($arTask['PARENT_ID']) && $arTask['PARENT_ID'] > 0) {

                    $paerntId = self::GetParentTaskId($arTask['PARENT_ID']);
                } else {
                    $paerntId = '';
                }

                $arNewTask = [];
                $arNewTask = [
                    'TITLE' => $arTask['TITLE'],
                    'DESCRIPTION' => $description,
                    'DEADLINE' => (isset($arTask['DEADLINE']) && strlen($arTask['DEADLINE']) > 1) ? date('Y-m-d\TH:i:sP', strtotime($arTask['DEADLINE'])) : '',
                    'START_DATE_PLAN' => (isset($arTask['START_DATE_PLAN']) && strlen($arTask['START_DATE_PLAN']) > 1) ? date('Y-m-d\TH:i:sP', strtotime($arTask['START_DATE_PLAN'])) : date('Y-m-d\TH:i:sP', time()),
                    'END_DATE_PLAN' => (isset($arTask['END_DATE_PLAN']) && strlen($arTask['END_DATE_PLAN']) > 1) ? date('Y-m-d\TH:i:sP', strtotime($arTask['END_DATE_PLAN'])) : date('Y-m-d\TH:i:sP', time()),
                    'PRIORITY' => $arTask['PRIORITY'],
                    'ACCOMPLICES' => $arAcomplices,
                    'AUDITORS' => $arAuditors,
                    'TAGS' => $arTask['TAGS'],
                    'TAG' => $arTask['TAG'],
                    'ALLOW_CHANGE_DEADLINE' => $arTask['ALLOW_CHANGE_DEADLINE'],
                    'TASK_CONTROL' => $arTask['TASK_CONTROL'],
                    'PARENT_ID' => $paerntId,
                    'DEPENDS_ON' => $arTask['DEPENDS_ON'],
                    'GROUP_ID' => $arTask['GROUP_ID'],
                    'RESPONSIBLE_ID' => self::getRemoteUserId($arTask['RESPONSIBLE_ID']),
                    'TIME_ESTIMATE' => $arTask['TIME_ESTIMATE'],
                    'CREATED_BY' => self::getRemoteUserId($arTask['CREATED_BY']),
                    'DECLINE_REASON' => $arTask['DECLINE_REASON'],
                    'REAL_STATUS' => $arTask['REAL_STATUS'],
                    'STATUS' => $arTask['STATUS'],
                    'CHANGED_BY' => self::getRemoteUserId($arTask['CHANGED_BY']),
                    'ALLOW_TIME_TRACKING' => $arTask['ALLOW_TIME_TRACKING'],
                    'SITE_ID' => $arTask['SITE_ID'],
                    'UF_MEDIAS_TASK_REMOTE_ID' => intval($arTask['ID'])
                ];

                $arParams['params']['arNewTaskData'] = $arNewTask;

                $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

                $arResult = \Bitrix\Main\Web\Json::decode($arResult);

                $taskLocalId = intval($arTask['ID']);

                $taskRemoteId = intval($arResult['result']);

                if($taskRemoteId == 0){

                    $arTask['GROUP_ID'] = self::LOCAL_GROUP_ID;

                    \Bitrix\Main\Loader::includeModule('im');
                                        
                    $fields = array(
                        "TO_USER_ID" => $arTask['CREATED_BY'],
                        "FROM_USER_ID" => 0,
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                        "NOTIFY_MODULE" => "sender",
                        "NOTIFY_MESSAGE" => 'Задача не була відправлена на віддалений сервер з технічних причин'
                        );
                    
                    \CIMNotify::Add($fields);
                    
                    $arLog = ['date' => date('d.m.Y H:i:s'), 'localTask' => $taskLocalId,'error' => $arResult];
                    
                    $strLog = \Bitrix\Main\Web\Json::encode($arLog);
                    
                    \Medias\Main\General\Handler::syncLog($strLog);

                    return;
                }

                self::addTaskToRegister($taskLocalId, $taskRemoteId);

                /**
                 * **********************************************************
                 */
                /**
                 * *
                 * add files in task
                 */
                if (isset($arTask['UF_TASK_WEBDAV_FILES']) && ! empty($arTask['UF_TASK_WEBDAV_FILES'])) {

                    $arParamsFile = [];
                    $arResultFile = [];

                    $arParamsFile['url'] = self::$restUrl . 'rest/' . self::$hookTaskAddFileCode . 'task.item.addfile.json';
                    $arParamsFile['followlocation'] = true;
                    $arParamsFile['ssl_ignore'] = true;

                    $arParamsFile['params']['TASK_ID'] = intval($taskRemoteId);

                    foreach ($arTask['UF_TASK_WEBDAV_FILES'] as $attachedObjectIdOrigin) {

                        if (strlen($attachedObjectIdOrigin) > 1) {

                            if (strpos($attachedObjectIdOrigin, 'n') !== false) {
                                $notAttachedObjectId = substr($attachedObjectIdOrigin, 1);

                                $arRes = \Bitrix\Disk\Internals\AttachedObjectTable::getList([
                                    'select' => [
                                        'ID'
                                    ],
                                    'filter' => [
                                        '=OBJECT_ID' => $notAttachedObjectId
                                    ]
                                ])->fetch();
                                
                                $attachedObjectId = $arRes['ID'];
                                
                                $obList = new \Medias\Main\General\Hl('m_task_file_register');

                                $res = $obList->getList([
                                    'UF_LOCAL_FILE_ID' => $attachedObjectId
                                ]);

                                if (empty($res)) {

                                    $arFile = self::getFileFromDiskData($notAttachedObjectId, $taskLocalId); // get file data

                                    $arParamsFile['params']['$fileParameters[NAME]'] = $arFile['name'];
                                    $arParamsFile['params']['$fileParameters[CONTENT]'] = $arFile['data'];

                                    $arResultFile = \Medias\Main\Integration\Rest::callCurl($arParamsFile);

                                    $arResultFile = \Bitrix\Main\Web\Json::decode($arResultFile);

                                    if ($arResultFile['result']['ATTACHMENT_ID']) {
                                        // need to parse DESCRIPTION
                                        $description = self::changeFileBbc($description, $attachedObjectIdOrigin, $arResultFile['result']['ATTACHMENT_ID']);

                                        $obList->addElementToHl([
                                            'UF_LOCAL_FILE_ID' => $attachedObjectId,
                                            'UF_REMOTE_FILE_ID' => $arResultFile['result']['ATTACHMENT_ID']
                                        ], [
                                            'UF_LOCAL_FILE_ID' => $attachedObjectId
                                        ]);

                                        $arParamsRemoteRegister['url'] = self::$restUrl . 'rest/' . self::$restFileSendMedias . 'medias.main.attach.register.add.json';
                                        $arParamsRemoteRegister['followlocation'] = true;
                                        $arParamsRemoteRegister['ssl_ignore'] = true;
                                        $arParamsRemoteRegister['params']['attacheId'] = intval($arResultFile['result']['ATTACHMENT_ID']);
                                        $arParamsRemoteRegister['params']['remoteFileId'] = intval($attachedObjectId);

                                        $arResultRegisterFile = \Medias\Main\Integration\Rest::callCurl($arParamsRemoteRegister);

                                        $arResultRegisterFile = \Bitrix\Main\Web\Json::decode($arResultRegisterFile);
                                    } else {
                                        
                                        \Bitrix\Main\Loader::includeModule('im');
                                        
                                        $fields = array(
                                            "TO_USER_ID" => $arTask['CREATED_BY'],
                                            "FROM_USER_ID" => 0,
                                            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                            "NOTIFY_MODULE" => "sender",
                                            // "NOTIFY_EVENT" => $imNotifyEvent,
                                            // "NOTIFY_TAG" => $notifyTag,
                                            "NOTIFY_MESSAGE" => 'Файл не відправлено'
                                        );
                                        \CIMNotify::Add($fields);

                                        $arLog = ['date' => date('d.m.Y H:i:s'), 'file' => $arFile['name'],'error' => $arResultFile];
                    
                                        $strLog = \Bitrix\Main\Web\Json::encode($arLog);
                                        
                                        \Medias\Main\General\Handler::syncLog($strLog);
                                    }
                                }

                                // add attache in remote register
                            }
                        }
                    }

                    $arParams = [];

                    $arParams['url'] = self::$restUrl . 'rest/' . self::$hookTaskUpdateCode . 'task.item.update.json';
                    $arParams['followlocation'] = true;
                    $arParams['ssl_ignore'] = true;

                    $arNewTaskNewDesc['UF_REMOTE_UPD'] = 'N';

                    $arNewTaskNewDesc['DESCRIPTION'] = $description;

                    $arParams['params']['id'] = intval($taskRemoteId);
                    $arParams['params']['arNewTaskData'] = $arNewTaskNewDesc;

                    $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

                    $arResult = \Bitrix\Main\Web\Json::decode($arResult);
                }

                $arTask['GROUP_ID'] = self::LOCAL_GROUP_ID;
            } else {

                /**
                 * *
                 * set remote task local id
                 */
                $addToRegister = true;
                $taskRemoteId = 0;
                $taskRemoteId = intval($arTask['UF_MEDIAS_TASK_REMOTE_ID']);
                $taskLocalId = $arTask['ID'];

                $obList = new \Medias\Main\General\Hl('m_task_medias_register');

                $arTaskRegisterRes = $obList->getList([
                    'UF_LOCAL_TASK_ID' => $taskLocalId,
					'UF_REMOTE_TASK_ID' => $taskRemoteId
                ]);

                if(empty($arTaskRegisterRes)){

                    $arTaskRegisterRes = $obList->getList([
                        'UF_LOCAL_TASK_ID' => $taskLocalId,
                    ]);

                    if(!empty($arTaskRegisterRes)){
                        if($arTaskRegisterRes[0]['UF_REMOTE_TASK_ID'] != $taskRemoteId){
                            $taskRemoteId = $arTaskRegisterRes[0]['UF_REMOTE_TASK_ID'];
                            $addToRegister = false;
                        }
                    }
                }

                $arParams = [];

                $arParams['url'] = self::$restUrl . 'rest/' . self::$hookTaskUpdateCode . 'task.item.update.json';
                $arParams['followlocation'] = true;
                $arParams['ssl_ignore'] = true;

                $arParams['params']['id'] = $taskRemoteId;
                $arParams['params']['arNewTaskData'] = [
                    'UF_MEDIAS_TASK_REMOTE_ID' => $taskLocalId,
                    'CHANGED_BY' => self::getRemoteUserId($arTask['CHANGED_BY'])
                ];

                if($addToRegister == true)
                    self::addTaskToRegister($taskLocalId, $taskRemoteId);

                \Medias\Main\Integration\Rest::callCurl($arParams);
            }
        }
    }

    public function checkIfTaskChanged($arNewFields, $arPrevFields)
    {
        $changed = false;

        if (isset($arNewFields['META:PREV_FIELDS']))
            unset($arNewFields['META:PREV_FIELDS']);

        foreach ($arNewFields as $fieldKey => $fieldValue) {

            if ($arPrevFields[$fieldKey] != $fieldValue)
                $changed = true;
        }

        return $changed;
    }

    public function setRemoteUpdated($localTaskId, $remoteTaskId, $remoteChange)
    {
        $obList = new \Medias\Main\General\Hl('m_task_medias_register');

        $resAdd = $obList->addElementToHl([
            'UF_REMOTE_UPDATE' => $remoteChange
        ], [
            'UF_LOCAL_TASK_ID' => $localTaskId,
            'UF_REMOTE_TASK_ID' => $remoteTaskId
        ]);

        return $resAdd;
    }

    public function OnBeforeTaskUpdate(int $taskId, array &$arTask, array &$arTaskCopy)
    {
        if ($arTaskCopy['GROUP_ID'] == self::LOCAL_GROUP_ID) {

            $remoteChange = boolval($arTask['UF_REMOTE_UPD']);

            $remoteTaskId = self::checkTaskInRegister($taskId);

            if (! empty($remoteTaskId) && $remoteChange == true) {

                self::setRemoteUpdated($taskId, $remoteTaskId[0]['UF_REMOTE_TASK_ID'], $remoteChange);
            } elseif (! empty($remoteTaskId) && $remoteChange != true) {

                self::setRemoteUpdated($taskId, $remoteTaskId[0]['UF_REMOTE_TASK_ID'], $remoteChange);
            }



            $arTask['UF_REMOTE_UPD'] = false;
        }
    }

    /**
     * **
     *
     * @param int $taskId
     * @param array $arTask
     */
    public function OnTaskUpdate(int $taskId, array &$arTask)
    {
        if (intval($arTask['META:PREV_FIELDS']['GROUP_ID']) == self::LOCAL_GROUP_ID) {

            \Bitrix\Main\Loader::includeModule('im');

            $remoteTaskId = self::checkTaskInRegister($taskId);

            if (! empty($remoteTaskId) && $remoteTaskId[0]['UF_REMOTE_UPDATE'] == false) {

                $arRemoteTask = self::getRemoteTaskData($remoteTaskId[0]['UF_REMOTE_TASK_ID'], $taskId);
                
                $arParams = [];

                $arParams['url'] = self::$restUrl . 'rest/' . self::$hookTaskUpdateCode . 'task.item.update.json';
                $arParams['followlocation'] = true;
                $arParams['ssl_ignore'] = true;

                $arTask['GROUP_ID'] = self::REMOTE_GROUP_ID;

                $arNewTask = [];

                if (isset($arTask['TITLE']))
                    $arNewTask['TITLE'] = $arTask['TITLE'];
                /*
                 * else
                 * $arNewTask['TITLE'] = $arTask['META:PREV_FIELDS']['TITLE'];
                 */

                if (isset($arTask['DESCRIPTION'])) {

                    $description = self::checkText($arTask['DESCRIPTION']);

                    // $description = self::checkTextFile($description, $arTask['CREATED_BY']);

                    $arNewTask['DESCRIPTION'] = $description;
                } /*
                   * else {
                   * $arNewTask['DESCRIPTION'] = $arTask['META:PREV_FIELDS']['DESCRIPTION'];
                   * }
                   */

                if (isset($arTask['DEADLINE']) && $arTask['DEADLINE'] != NULL)
                    $arNewTask['DEADLINE'] = date('Y-m-d\TH:i:sP', strtotime($arTask['DEADLINE']));
                /*
                 * else
                 * $arNewTask['DEADLINE'] = ($arTask['META:PREV_FIELDS']['DEADLINE'] == NULL) ? '' : date('Y-m-d\TH:i:sP', strtotime($arTask['META:PREV_FIELDS']['DEADLINE']));
                 */
                if (isset($arTask['START_DATE_PLAN']) && $arTask['START_DATE_PLAN'] != NULL)
                    $arNewTask['START_DATE_PLAN'] = date('Y-m-d\TH:i:sP', strtotime($arTask['START_DATE_PLAN']));
                /*
                 * else
                 * $arNewTask['START_DATE_PLAN'] = ($arTask['META:PREV_FIELDS']['START_DATE_PLAN'] == NULL) ? '' : date('Y-m-d\TH:i:sP', strtotime($arTask['META:PREV_FIELDS']['START_DATE_PLANs']));
                 */
                if (isset($arTask['END_DATE_PLAN']) && $arTask['END_DATE_PLAN'] != NULL)
                    $arNewTask['END_DATE_PLAN'] = date('Y-m-d\TH:i:sP', strtotime($arTask['END_DATE_PLAN']));
                /*
                 * else
                 * $arNewTask['END_DATE_PLAN'] = ($arTask['META:PREV_FIELDS']['END_DATE_PLAN'] == NULL) ? '' : date('Y-m-d\TH:i:sP', strtotime($arTask['META:PREV_FIELDS']['END_DATE_PLAN']));
                 */
                if (isset($arTask['PRIORITY']))
                    $arNewTask['PRIORITY'] = $arTask['PRIORITY'];
                /*
                 * else
                 * $arNewTask['PRIORITY'] = $arTask['META:PREV_FIELDS']['PRIORITY'];
                 */
                if (isset($arTask['ACCOMPLICES'])) {

                    if (is_array($arTask['ACCOMPLICES'])) {
                        foreach ($arTask['ACCOMPLICES'] as $accomplicer) {

                            $remoteAccomplicer = self::getRemoteUserId($accomplicer);

                            if ($remoteAccomplicer != self::$TECH_USER){
                                if($remoteAccomplicer != 1){
                                    $arNewTask['ACCOMPLICES'][] = $remoteAccomplicer;
                                }
                            }
                                
                        }

                        if (is_array($arRemoteTask['ACCOMPLICES'])) {
                            $arNewTask['ACCOMPLICES'] = array_merge($arRemoteTask['ACCOMPLICES'],$arNewTask['ACCOMPLICES']);
                            $arNewTask['ACCOMPLICES'] = array_unique($arNewTask['ACCOMPLICES']);
                        }
                    } else {

                        $remoteAccomplicer = self::getRemoteUserId($arTask['ACCOMPLICES']);

                        if ($remoteAccomplicer != self::$TECH_USER){
                            if($remoteAccomplicer != 1){
                                $arNewTask['ACCOMPLICES'] = $remoteAccomplicer;
                            }
                        }

                        if (is_array($arRemoteTask['ACCOMPLICES'])) {
                            $arRemoteTask['ACCOMPLICES'] += $arNewTask['ACCOMPLICES'];
                            $arNewTask['ACCOMPLICES'] = array_unique($arRemoteTask['ACCOMPLICES']);
                        }
                    }
                }

                if (isset($arTask['ACCOMPLICE']))
                    $arNewTask['ACCOMPLICE'] = $arTask['ACCOMPLICE'];
                /*
                 * else
                 * $arNewTask['ACCOMPLICE'] = $arTask['META:PREV_FIELDS']['ACCOMPLICE'];
                 */
                if (isset($arTask['AUDITORS'])) {

                    if (is_array($arTask['AUDITORS'])) {

                        foreach ($arTask['AUDITORS'] as $auditor) {

                            $remoteAuditor = self::getRemoteUserId($auditor);

                            if ($remoteAuditor != self::$TECH_USER){
                                if($remoteAuditor != 1){
                                    $arNewTask['AUDITORS'][] = $remoteAuditor;
                                }
                            }
                        }

                        if (is_array($arRemoteTask['AUDITORS'])) {
                            $arNewTask['AUDITORS'] = array_merge($arRemoteTask['AUDITORS'],$arNewTask['AUDITORS']);
                            $arNewTask['AUDITORS'] = array_unique($arNewTask['AUDITORS']);
                        }
                    } else {

                        $remoteAuditor = self::getRemoteUserId($arTask['AUDITORS']);

                        if ($remoteAuditor != self::$TECH_USER){
                            if($remoteAuditor != 1){
                                $arNewTask['AUDITORS'] = $remoteAuditor;
                            }
                        }

                        if (is_array($arRemoteTask['AUDITORS'])) {
                            $arRemoteTask['AUDITORS'] = array_merge($arRemoteTask['AUDITORS'],$arNewTask['AUDITORS']);
                            $arNewTask['AUDITORS'] = array_unique($arRemoteTask['AUDITORS']);
                        }
                    }
                } 

                if (isset($arTask['AUDITOR']))
                    $arNewTask['AUDITOR'] = $arTask['AUDITOR'];
                /*
                 * else
                 * $arNewTask['AUDITOR'] = $arTask['META:PREV_FIELDS']['AUDITOR'];
                 */
                if (isset($arTask['TAGS']))
                    $arNewTask['TAGS'] = $arTask['TAGS'];
                /*
                 * else
                 * $arNewTask['TAGS'] = $arTask['META:PREV_FIELDS']['TAGS'];
                 */
                if (isset($arTask['ALLOW_CHANGE_DEADLINE']))
                    $arNewTask['ALLOW_CHANGE_DEADLINE'] = $arTask['ALLOW_CHANGE_DEADLINE'];
                /*
                 * else
                 * $arNewTask['ALLOW_CHANGE_DEADLINE'] = $arTask['META:PREV_FIELDS']['ALLOW_CHANGE_DEADLINE'];
                 */
                if (isset($arTask['ALLOW_CHANGE_DEADLINE']))
                    $arNewTask['ALLOW_CHANGE_DEADLINE'] = $arTask['ALLOW_CHANGE_DEADLINE'];
                /*
                 * else
                 * $arNewTask['ALLOW_CHANGE_DEADLINE'] = $arTask['META:PREV_FIELDS']['ALLOW_CHANGE_DEADLINE'];
                 */
                if (isset($arTask['TASK_CONTROL']))
                    $arNewTask['TASK_CONTROL'] = $arTask['TASK_CONTROL'];
                /*
                 * else
                 * $arNewTask['TASK_CONTROL'] = $arTask['META:PREV_FIELDS']['TASK_CONTROL'];
                 */
                if (isset($arTask['PARENT_ID']) && $arTask['PARENT_ID'] > 0) {

                    $paerntId = self::GetParentTaskId($arTask['PARENT_ID']);

                    $arNewTask['PARENT_ID'] = $paerntId;
                } /*
                   * else {
                   * $arNewTask['PARENT_ID'] = $arTask['META:PREV_FIELDS']['PARENT_ID'];
                   * }
                   */

                if (isset($arTask['DEPENDS_ON']))
                    $arNewTask['DEPENDS_ON'] = $arTask['DEPENDS_ON'];
                /*
                 * else
                 * $arNewTask['DEPENDS_ON'] = $arTask['META:PREV_FIELDS']['DEPENDS_ON'];
                 */
                if (isset($arTask['RESPONSIBLE_ID']))
                    $arNewTask['RESPONSIBLE_ID'] = self::getRemoteUserId($arTask['RESPONSIBLE_ID']);
                /*
                 * else
                 * $arNewTask['RESPONSIBLE_ID'] = self::getRemoteUserId($arTask['META:PREV_FIELDS']['RESPONSIBLE_ID']);
                 */
                if (isset($arTask['TIME_ESTIMATE']))
                    $arNewTask['TIME_ESTIMATE'] = $arTask['TIME_ESTIMATE'];
                /*
                 * else
                 * $arNewTask['TIME_ESTIMATE'] = $arTask['META:PREV_FIELDS']['TIME_ESTIMATE'];
                 */
                if (isset($arTask['CREATED_BY']))
                    $arNewTask['CREATED_BY'] = self::getRemoteUserId($arTask['CREATED_BY']);
                else
                    $arNewTask['CREATED_BY'] = self::getRemoteUserId($arTask['META:PREV_FIELDS']['CREATED_BY']);

                if (isset($arTask['DECLINE_REASON']))
                    $arNewTask['DECLINE_REASON'] = $arTask['DECLINE_REASON'];
                else
                    $arNewTask['DECLINE_REASON'] = $arTask['META:PREV_FIELDS']['DECLINE_REASON'];

                if (isset($arTask['STATUS']))
                    $arNewTask['STATUS'] = $arTask['STATUS'];
                else
                    $arNewTask['STATUS'] = $arTask['META:PREV_FIELDS']['STATUS'];

                $arNewTask['UF_REMOTE_UPD'] = 'Y';

                $arParams['params']['id'] = intval($remoteTaskId[0]['UF_REMOTE_TASK_ID']);
                $arParams['params']['arNewTaskData'] = $arNewTask;

                $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

                $arResult = \Bitrix\Main\Web\Json::decode($arResult);
                
                if($arResult['result'] != NULL){
                    
                    $arLog = \Bitrix\Main\Web\Json::encode($arResult);
                
                    \Medias\Main\General\Handler::syncLog($arLog);
                }

                if (isset($arTask['UF_TASK_WEBDAV_FILES'])) {

                    $arParamsFile = [];
                    $arResultFile = [];

                    $arParamsFile['url'] = self::$restUrl . 'rest/' . self::$hookTaskAddFileCode . 'task.item.addfile.json';
                    $arParamsFile['followlocation'] = true;
                    $arParamsFile['ssl_ignore'] = true;

                    $arParamsFile['params']['TASK_ID'] = intval($remoteTaskId[0]['UF_REMOTE_TASK_ID']);
                    
                    foreach ($arTask['UF_TASK_WEBDAV_FILES'] as $attachedObjectIdOrigin) {
                     
                        if (strlen($attachedObjectIdOrigin) > 0) {
                            
                            // $attachedObjectId - file on disk but not attached
                            if (strpos($attachedObjectIdOrigin, 'n') !== false) {
                                $notAttachedObjectId = substr($attachedObjectIdOrigin, 1);

                                $arRes = \Bitrix\Disk\Internals\AttachedObjectTable::getList([
                                    'select' => [
                                        'ID'
                                    ],
                                    'filter' => [
                                        '=OBJECT_ID' => $notAttachedObjectId
                                    ]
                                ])->fetch();
                                
                                $attachedObjectId = $arRes['ID'];
                                
                                $obList = new \Medias\Main\General\Hl('m_task_file_register');

                                $res = $obList->getList([
                                    'UF_LOCAL_FILE_ID' => $attachedObjectId
                                ]);
                                
                                if (empty($res)) {

                                    $arFile = self::getFileFromDiskData($notAttachedObjectId, $taskId);

                                    $arParamsFile['params']['$fileParameters[NAME]'] = $arFile['name'];
                                    $arParamsFile['params']['$fileParameters[CONTENT]'] = $arFile['data'];

                                    $arResultFile = \Medias\Main\Integration\Rest::callCurl($arParamsFile);

                                    $arResultFile = \Bitrix\Main\Web\Json::decode($arResultFile);
                                  
                                    if ($arResultFile['result']['ATTACHMENT_ID']) {
                                        // need to parse DESCRIPTION
                                        
                                        $description = self::changeFileBbc($description, $attachedObjectIdOrigin, $arResultFile['result']['ATTACHMENT_ID']);

                                        $obList->addElementToHl([
                                            'UF_LOCAL_FILE_ID' => $attachedObjectId,
                                            'UF_REMOTE_FILE_ID' => $arResultFile['result']['ATTACHMENT_ID']
                                        ], [
                                            'UF_LOCAL_FILE_ID' => $attachedObjectId
                                        ]);

                                        $arParamsRemoteRegister['url'] = self::$restUrl . 'rest/' . self::$restFileSendMedias . 'medias.main.attach.register.add.json';
                                        $arParamsRemoteRegister['followlocation'] = true;
                                        $arParamsRemoteRegister['ssl_ignore'] = true;
                                        $arParamsRemoteRegister['params']['attacheId'] = intval($arResultFile['result']['ATTACHMENT_ID']);
                                        $arParamsRemoteRegister['params']['remoteFileId'] = intval($attachedObjectId);

                                        $arResultRegisterFile = \Medias\Main\Integration\Rest::callCurl($arParamsRemoteRegister);

                                        $arResultRegisterFile = \Bitrix\Main\Web\Json::decode($arResultRegisterFile);
                                    } else {

                                        \Bitrix\Main\Loader::includeModule('im');
                                        
                                        $fields = array(
                                            "TO_USER_ID" => $arTask['CHANGED_BY'],
                                            "FROM_USER_ID" => 0,
                                            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                            "NOTIFY_MODULE" => "sender",
                                            // "NOTIFY_EVENT" => $imNotifyEvent,
                                            // "NOTIFY_TAG" => $notifyTag,
                                            "NOTIFY_MESSAGE" => 'Файл не відправлено'
                                        );
                                        \CIMNotify::Add($fields);
                                        
                                        $fields['error'] = $arResultFile;

                                        $arLog = \Bitrix\Main\Web\Json::encode($fields);
                                        
                                        \Medias\Main\General\Handler::syncLog($arLog);
                                    }
                                }

                                // add attache in remote register
                            } else { // without 'n' - file is allready attached to task
                                $attachedObjectId = $attachedObjectIdOrigin;

                                $arRes = \Bitrix\Disk\Internals\AttachedObjectTable::getList([
                                    'select' => [
                                        'OBJECT_ID'
                                    ],
                                    'filter' => [
                                        '=ID' => $attachedObjectId
                                    ]
                                ])->fetch();
                                    
                                $attachedObjectInString = 'n'.$arRes['ID'];

                                // $fileAttachId = self::getFileIdByAttach($attachedObjectId);

                                $obList = new \Medias\Main\General\Hl('m_task_file_register');

                                $res = $obList->getList([
                                    'UF_LOCAL_FILE_ID' => $attachedObjectId
                                ]);
                                
                                if ($res) {

                                    $remotrAttacheId = $res[0]['UF_REMOTE_FILE_ID'];

                                    $arParamsRemoteObject['url'] = self::$restUrl . 'rest/' . self::$restFileSendMedias . 'medias.main.attach.get.byobject.json';
                                    $arParamsRemoteObject['followlocation'] = true;
                                    $arParamsRemoteObject['ssl_ignore'] = true;
                                    $arParamsRemoteObject['params']['objectId'] = intval($remotrAttacheId);

                                    $arResultObjectFile = \Medias\Main\Integration\Rest::callCurl($arParamsRemoteObject);

                                    $arResultObjectFile = \Bitrix\Main\Web\Json::decode($arResultObjectFile);

                                    $remoteAttachId = $arResultObjectFile['result']['ID']; 
                                    // $remoteAttachId = $remoteObjectId;
                                    if ($remoteAttachId) {
                                        // need to parse DESCRIPTION
                                        $description = self::changeFileBbc($description, $attachedObjectInString, $remotrAttacheId);
                                    } else {
                                      
                                        \Bitrix\Main\Loader::includeModule('im');
                                        
                                        $fields = array(
                                            "TO_USER_ID" => $arTask['CHANGED_BY'],
                                            "FROM_USER_ID" => 0,
                                            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                            "NOTIFY_MODULE" => "sender",
                                            // "NOTIFY_EVENT" => $imNotifyEvent,
                                            // "NOTIFY_TAG" => $notifyTag,
                                            "NOTIFY_MESSAGE" => 'Файл не відправлено'
                                        );
                                        \CIMNotify::Add($fields);
                                        
                                        $arLog = \Bitrix\Main\Web\Json::encode($fields);
                                        
                                        \Medias\Main\General\Handler::syncLog($arLog);
                                    }
                                } else {

                                    $arFile = self::getFileFromDiskData($attachedObjectId, $taskId);

                                    $arParamsFile['params']['$fileParameters[NAME]'] = $arFile['name'];
                                    $arParamsFile['params']['$fileParameters[CONTENT]'] = $arFile['data'];

                                    $arParamsFile['url'] = self::$restUrl . 'rest/' . self::$hookTaskAddFileCode . 'task.item.addfile.json';
                                    $arParamsFile['followlocation'] = true;
                                    $arParamsFile['ssl_ignore'] = true;

                                    $arResultFile = \Medias\Main\Integration\Rest::callCurl($arParamsFile);

                                    $arResultFile = \Bitrix\Main\Web\Json::decode($arResultFile);
                                    
                                    if ($arResultFile['result']['ATTACHMENT_ID']) {
                                        // need to parse DESCRIPTION
                                        $description = self::changeFileBbc($description, $attachedObjectIdOrigin, $arResultFile['result']['ATTACHMENT_ID']);

                                        $obList->addElementToHl([
                                            'UF_LOCAL_FILE_ID' => $attachedObjectId,
                                            'UF_REMOTE_FILE_ID' => $arResultFile['result']['ATTACHMENT_ID']
                                        ], [
                                            'UF_LOCAL_FILE_ID' => $attachedObjectId
                                        ]);
                                    } else {
                                        
                                        \Bitrix\Main\Loader::includeModule('im');
                                        
                                        $fields = array(
                                            "TO_USER_ID" => $arTask['CHANGED_BY'],
                                            "FROM_USER_ID" => 0,
                                            "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                            "NOTIFY_MODULE" => "sender",
                                            // "NOTIFY_EVENT" => $imNotifyEvent,
                                            // "NOTIFY_TAG" => $notifyTag,
                                            "NOTIFY_MESSAGE" => 'Файл не відправлено'
                                        );
                                        \CIMNotify::Add($fields);
                                        
                                        $arLog = \Bitrix\Main\Web\Json::encode($fields);
                                        
                                        \Medias\Main\General\Handler::syncLog($arLog);
                                    }
                                }
                            }
                        }
                    }

                    $arNewTask['UF_REMOTE_UPD'] = 'Y';

                    $arNewTask['DESCRIPTION'] = $description;

                    $arParams['params']['id'] = intval($remoteTaskId[0]['UF_REMOTE_TASK_ID']);
                    $arParams['params']['arNewTaskData'] = $arNewTask;

                    $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

                    $arResult = \Bitrix\Main\Web\Json::decode($arResult);
                }
            }
            $arTask['GROUP_ID'] = self::LOCAL_GROUP_ID;
        }
    }

    protected function getFileIdByAttach($attacheId)
    {
        $arRes = [];

        $arRes = \Bitrix\Disk\AttachedObject::getById($attacheId)->toArray();

        /*
         * $arObjects = \Bitrix\Disk\Internals\ObjectTable::getList([
         * 'select' => ['NAME', 'FILE_ID','ID'],
         * 'filter' => [
         * '=ID' => $arRes['OBJECT_ID'],
         * ]
         * ])->fetch();
         */

        return $arRes['OBJECT_ID'];
    }

    protected function checkUserInRegister(int $userId)
    {
        $arParams = [];
        $arResult = [];

        $arParams['url'] = self::$restUrl . 'rest/' . self::$hookUserGetCode . 'medias.main.user.register.check.json';
        $arParams['followlocation'] = true;
        $arParams['ssl_ignore'] = true;
        $arParams['params']['$userId'] = intval($userId);

        $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

        $arResult = \Bitrix\Main\Web\Json::decode($arResult);

        return $arResult;
    }

    protected function getRemoteTaskData(int $taskId = null, int $localTaskId)
    {
        $arParams = [];
        $arResult = [];

        if(intval($taskId) == 0){
            
            $obList = new \Medias\Main\General\Hl('m_task_medias_register');
            
            $arResult = $obList->getList([
                'UF_LOCAL_TASK_ID' => $localTaskId
            ]);
            
            $taskId =  $arResult[0]['UF_REMOTE_TASK_ID'];
        }

        $arParams['url'] = self::$restUrl . 'rest/' . self::$hookTaskUpdateCode . 'task.item.getdata.json';
        $arParams['followlocation'] = true;
        $arParams['ssl_ignore'] = true;
        $arParams['params']['TASK_ID'] = intval($taskId);

        $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

        $arResult = \Bitrix\Main\Web\Json::decode($arResult);

        return $arResult['result'];
    }

    /**
     * *
     *
     * @param int $taskId
     * @return mixed
     */
    protected function GetParentTaskId(int $taskId)
    {
        $arResult = [];

        $obList = new \Medias\Main\General\Hl('m_task_medias_register');

        $arResult = $obList->getList([
            'UF_LOCAL_TASK_ID' => $taskId
        ]);

        return $arResult[0]['UF_REMOTE_TASK_ID'];
    }

    /**
     * ***
     *
     * @param string $entityType
     * @param int $taskId
     * @param array $arData
     */
    public function OnTaskCommentAdd($entityType, $taskId, $arData)
    {
        if ($arData['AUX_DATA'] == '') {
            $remoteTaskId = self::checkTaskInRegister($taskId);

            if ($remoteTaskId) {

                $remoteSyncRestSync = \Bitrix\Main\Config\Option::get("medias.main", "rest_to_higload");

                \Bitrix\Main\Loader::includeModule('forum');

                \Bitrix\Main\Loader::includeModule('im');

                // отримуємо віддалену задачу
                $obList = new \Medias\Main\General\Hl('m_task_medias_register');

                $arResultRegister = $obList->getList([
                    'UF_LOCAL_TASK_ID' => $taskId
                ]);

                // в хайлоад віддалений локальний id
                $arParams = [];
                $arResult = [];

                $arParams['url'] = self::$restUrl . 'rest/' . $remoteSyncRestSync . 'medias.tasks.comments.register.add.json';
                $arParams['params']['fields']['UF_TASK_LOCAL_ID'] = $arResultRegister[0]['UF_REMOTE_TASK_ID'];
                $arParams['params']['fields']['UF_COMMENT_REMOTE_ID'] = $arData['MESSAGE_ID'];
                $arParams['ssl_ignore'] = true;

                $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

                $arResult = \Bitrix\Main\Web\Json::decode($arResult);

                $arParams = [];
                $arResult = [];

                $message = self::checkText($arData['PARAMS']['POST_MESSAGE']);

                // $message = self::changeFileBbc($message, );

                $arParams['url'] = self::$restUrl . 'rest/' . $remoteSyncRestSync . 'medias.tasks.comments.add.json';

                $arParams['params']['fields']['POST_MESSAGE'] = $message;
                $arParams['params']['fields']['AUTHOR_ID'] = self::getRemoteUserId($arData['PARAMS']['AUTHOR_ID']);
                $arParams['params']['fields']['TASK_ID'] = $remoteTaskId[0]['UF_REMOTE_TASK_ID'];
                $arParams['params']['fields']['UF_COMMENT_ID'] = $arData['MESSAGE_ID'];
                $arParams['ssl_ignore'] = true;

                $arResult = \Medias\Main\Integration\Rest::callCurl($arParams);

                $arResult = \Bitrix\Main\Web\Json::decode($arResult);

                if(!isset($arResult['result']['error']))
                    $remoteCommentId = $arResult['result'];
                else    
                    $remoteCommentId = false;

                if ($remoteCommentId !== false) {
                    self::addCommentRegister($taskId, $arData['MESSAGE_ID'], $remoteCommentId);

                    /**
                     * ***
                     * add files from comment to task
                     */
                    if (isset($arData['PARAMS']['UF_FORUM_MESSAGE_DOC']) && ! empty($arData['PARAMS']['UF_FORUM_MESSAGE_DOC'])) {

                        $arParamsFile = [];
                        $arResultFile = [];

                        $arParamsFile['url'] = self::$restUrl . 'rest/' . $remoteSyncRestSync . 'medias.main.uploadfile.json';
                        $arParamsFile['followlocation'] = true;
                        $arParamsFile['ssl_ignore'] = true;

                        // $arParamsFile['params']['TASK_ID'] = intval($remoteTaskId[0]['UF_REMOTE_TASK_ID']);
                        $arParamsFile['params']['entityId'] = intval($remoteCommentId);
                        $arParamsFile['params']['moduleId'] = 'forum';

                        foreach ($arData['PARAMS']['UF_FORUM_MESSAGE_DOC'] as $attachedObjectIdOrigin) {

                            if (strlen($attachedObjectIdOrigin) > 1) {

                                if (strpos($attachedObjectIdOrigin, 'n') !== false)
                                    $attachedObjectId = substr($attachedObjectIdOrigin, 1);

                                $arFile = self::getFileFromDiskData($attachedObjectId, $taskId);

                                if(strlen($arFile['data']) > 1){

                                    $arFile['PATH'] = $arFile['src'];

                                    $arFile['FULL_PATH'] = 'https://' . $_SERVER['SERVER_NAME'] . $arFile['PATH'];
                                    $arFile['DATA'] = $arFile['data'];
                                }else{
                                    $arFile['PATH'] = $arFile['src'];

                                    $arFile['FULL_PATH'] = $arFile['src'];
                                    $arFile['DATA'] = $arFile['data'];
                                }

                                $arParamsFile['params']['file'] = $arFile;
                                $remoteUserId = self::getRemoteUserId($arData['PARAMS']['AUTHOR_ID']);

                                $arParamsFile['params']['userId'] = $remoteUserId;

                                $arResultFile = \Medias\Main\Integration\Rest::callCurl($arParamsFile);

                                $arResultFile = \Bitrix\Main\Web\Json::decode($arResultFile);

                                if($arResultFile['result']['error'] == true){
                                    
                                    $arLog = \Bitrix\Main\Web\Json::encode($arResultFile);
                                    
                                    \Medias\Main\General\Handler::syncLog($arLog);
                                }
                                
                                $remoteDiskFile = $arResultFile['result']['attachedModelId'];
                                $remoteFile = $arResultFile['result']['objectId'];

                                if ($remoteDiskFile) {
                                    $message = self::changeFileBbc($message, $attachedObjectIdOrigin, $remoteDiskFile);

                                    $arRemoteFiles[] = $remoteDiskFile;

                                    $arParamsComment['url'] = self::$restUrl . 'rest/' . $remoteSyncRestSync . 'medias.tasks.comments.update.json';
                                    $arParamsComment['followlocation'] = true;
                                    $arParamsComment['ssl_ignore'] = true;

                                    $arParamsComment['params']['FIELDS'] = [
                                        'POST_MESSAGE' => $message,
                                        'USE_SMILES' => 'Y',
                                        // 'FILES' => $arRemoteFiles,
                                        'TASK_ID' => intval($remoteTaskId[0]['UF_REMOTE_TASK_ID']),
                                        'UF_COMMENT_ID' => intval($remoteCommentId),
                                        'UF_FORUM_MESSAGE_DOC' => $arRemoteFiles
                                    ];

                                    $arResultComment = \Medias\Main\Integration\Rest::callCurl($arParamsComment);
                                } else {

                                    \Bitrix\Main\Loader::includeModule('im');
                                    
                                    $fields = array(
                                        "TO_USER_ID" => $arData['PARAMS']['AUTHOR_ID'],
                                        "FROM_USER_ID" => 0,
                                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                        "NOTIFY_MODULE" => "sender",
                                        // "NOTIFY_EVENT" => $imNotifyEvent,
                                        // "NOTIFY_TAG" => $notifyTag,
                                        "NOTIFY_MESSAGE" => 'Файл не відправлено'
                                    );
                                    if(\CIMNotify::Add($fields)){
                                    
                                        $arLog = \Bitrix\Main\Web\Json::encode($fields);
                                        
                                        \Medias\Main\General\Handler::syncLog($arLog);

                                        return;
                                    }else{
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }else{

                    $fields = array(
                        "TO_USER_ID" => $arData['PARAMS']['AUTHOR_ID'],
                        "FROM_USER_ID" => 0,
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                        "NOTIFY_MODULE" => "sender",
                        // "NOTIFY_EVENT" => $imNotifyEvent,
                        // "NOTIFY_TAG" => $notifyTag,
                        "NOTIFY_MESSAGE" => 'Коментар не відправленно на віддалений сервер'
                    );

                    if(\CIMNotify::Add($fields)){

                        $fields['taskId'] = $remoteTaskId[0]['UF_REMOTE_TASK_ID'];
                        $fields['message'] = $message;

                        $arLog = \Bitrix\Main\Web\Json::encode($fields);
                                        
                        \Medias\Main\General\Handler::syncLog($arLog);
                    }else{
                        return;
                    }
                }
            }
        }
    /**
     * ****************end files***********************
     */
    }

    /**
     * ****
     *
     * @param int $commentId
     * @param array $arData
     * @param int $taskId
     */
    public function OnAfterCommentUpdate($entityType, $taskId, $arData)
    {
        return;
        $remoteTaskId = self::checkTaskInRegister($taskId);

        if ($remoteTaskId) {

            $remoteCommentId = self::getCommentInRegister($taskId, $arData['MESSAGE_ID']);

            if ($remoteCommentId) {

                $arParamsComment = [];
                $arResultComment = [];

                $arParamsComment['url'] = self::$restUrl . 'rest/' . self::$hookTaskUpdateCommentItemCode . 'task.commentitem.update.json';
                $arParamsComment['followlocation'] = true;
                $arParamsComment['ssl_ignore'] = true;

                $arParamsComment['params']['TASKID'] = intval($remoteTaskId[0]['UF_REMOTE_TASK_ID']);
                $arParamsComment['params']['ITEMID'] = intval($remoteCommentId);

                $postMessage = self::checkText($arData['PARAMS']['POST_MESSAGE']);

                $postMessage = self::checkTextFile($postMessage, self::getRemoteUserId($arData['PARAMS']['AUTHOR_ID']));

                $arParamsComment['params']['FIELDS'] = [
                    'POST_MESSAGE' => $postMessage
                ];

                $arResultComment = \Medias\Main\Integration\Rest::callCurl($arParamsComment);
            }
        }
    }

    /**
     * *****
     *
     * @param int $userId
     * @return mixed
     */
    protected function getRemoteUserId(int $userId)
    {
        $remoteUser = 0;

        $obList = new \Medias\Main\General\Hl('m_remote_user');

        $arUser = $obList->getList([
            'UF_USER_ID' => $userId
        ]);

        if (empty($arUser)) {

            self::$TECH_USER = \Bitrix\Main\Config\Option::get('medias.main', 'tech_user');

            if (self::$TECH_USER != null)
                $remoteUser = self::$TECH_USER;
            else
                $remoteUser = 1;
        } else {
            $remoteUser = $arUser[0]['UF_REMOTE_USER_ID'];
        }

        return $remoteUser;
    }

    /**
     * ******
     *
     * @param int $userId
     * @return array
     */
    protected function getRemoteUserData(int $userId)
    {
        $obList = new \Medias\Main\General\Hl('m_remote_user');

        $arUser = $obList->getList([
            'UF_USER_ID' => $userId
        ]);

        return $arUser;
    }

    /**
     * *******
     *
     * @param int $taskId
     * @return array
     */
    protected function checkTaskInRegister(int $taskId)
    {
        $arResult = [];

        $obList = new \Medias\Main\General\Hl('m_task_medias_register');

        $arResult = $obList->getList([
            'UF_LOCAL_TASK_ID' => $taskId
        ]);

        return $arResult;
    }

    /**
     * ********
     *
     * @param int $taskLoacalId
     * @param int $taskRemoteId
     * @return boolean
     */
    protected function addTaskToRegister(int $taskLoacalId, int $taskRemoteId)
    {
        $obList = new \Medias\Main\General\Hl('m_task_medias_register');

        $resAdd = $obList->addElementToHl([
            'UF_LOCAL_TASK_ID' => $taskLoacalId,
            'UF_REMOTE_TASK_ID' => $taskRemoteId
        ], [
            'UF_LOCAL_TASK_ID' => $taskLoacalId,
            'UF_REMOTE_TASK_ID' => $taskRemoteId
        ]);

        return $resAdd;
    }

    /**
     * *********
     *
     * @param int $taskRemoteId
     * @param int $taskCheckListLocalId
     * @param int $taskCheckListRemoteId
     * @return boolean
     */
    protected function addTaskCheckListItemToRegister(int $taskRemoteId, int $taskCheckListLocalId, int $taskCheckListRemoteId)
    {
        $obList = new \Medias\Main\General\Hl('m_task_medias_register');

        $resAdd = $obList->addElementToHl([
            'UF_TASK_REMOTE_ID' => $taskRemoteId,
            'UF_CH_LIST_LOCAL_ID' => $taskCheckListLocalId,
            'UF_CH_LIST_REMOTE_ID' => $taskCheckListRemoteId
        ], [
            'UF_TASK_REMOTE_ID' => $taskRemoteId,
            'UF_CH_LIST_LOCAL_ID' => $taskCheckListLocalId,
            'UF_CH_LIST_REMOTE_ID' => $taskCheckListRemoteId
        ]);

        return $resAdd;
    }

    /**
     * **********
     *
     * @param string|int $fileId
     * @return array
     */
    protected function getFileFromDiskData(&$fileId, $taskId)
    {
        $arObject = [];

        $arObject = \Bitrix\Disk\Internals\ObjectTable::getList([
            'filter' => [
                'ID' => $fileId
            ],
            'select' => [
                'NAME',
                'FILE_ID',
                'ID'
            ]
        ])->fetch();

        $applicarion = \Bitrix\Main\Application::getInstance();
        $documentRoot = $applicarion->getDocumentRoot();

        $fileName = $arObject['NAME'];
        $fileIdReal = $arObject['FILE_ID'];

        $fileSrc = \CFile::GetPath($fileIdReal);
        $fileData = file_get_contents($documentRoot.$fileSrc);

        $arObject['src'] = $fileSrc;
        $arObject['data'] = $fileData;
        $arObject['name'] = $fileName;

        if ($arObject['data'] != false) {

            $arObject['data'] = base64_encode($fileData);
        } else {
            $arObject['path'] = $arObject['src'];

            $arObject['full_path'] = $arObject['src'];
            $arObject['data'] = base64_encode(file_get_contents($arObject['full_path'], true));
        }

        return $arObject;
    }

    /**
     * ***********
     *
     * @param int $taskLocalId
     * @param int $messageLocalId
     * @param int $messageRemoteId
     * @return boolean
     */
    protected function addCommentRegister(int $taskLocalId, int $messageLocalId, int $messageRemoteId)
    {
        $arRegister = [];

        $arRegister['UF_TASK_LOCAL_ID'] = $taskLocalId;
        $arRegister['UF_COMMENT_ID'] = $messageLocalId;
        $arRegister['UF_COMMENT_REMOTE_ID'] = $messageRemoteId;

        $obList = new \Medias\Main\General\Hl('m_task_comment_register');

        $resAdd = $obList->addElementToHl($arRegister, [
            'UF_COMMENT_ID' => $messageLocalId
        ]);

        return $resAdd;
    }

    /**
     * **
     *
     * @param int $taskLocalId
     * @param int $messageLocalId
     * @return mixed
     */
    protected function getCommentInRegister(int $taskLocalId, int $messageLocalId)
    {
        $arResult = [];

        $obList = new \Medias\Main\General\Hl('m_task_comment_register');

        $arResult = $obList->getList([
            'UF_COMMENT_ID' => $messageLocalId
        ]);

        return $arResult[0]['UF_COMMENT_REMOTE_ID'];
    }

    /**
     * **
     *
     * @param string $text
     * @return string
     */
    protected function checkText(string &$text)
    {
        $startString = '[USER=';
        $indexBbcStart = strpos($text, $startString);

        if ($indexBbcStart !== false) {

            $text = self::changeUserBbc($text);

            return $text;
        } else {
            return $text;
        }
    }

    public function checkTextFile(string &$text, string $oldAttId, string $newAttId)
    {
        $startString = '[DISK FILE ID=';

        $indexBbcStart = strpos($text, $startString . $newAttId);

        if ($indexBbcStart !== false) {

            $text = self::changeFileBbc($text, $newAttId);

            return $text;
        } else {
            return $text;
        }
    }

    /**
     * ************
     *
     * @param string $string
     * @return string
     */
    protected function changeUserBbc(string $string)
    {
        $strResult = '';

        $startString = '[USER=';
        $finishString = '[/USER]';

        /**
         * *
         * get first user
         */
        $indexBbcStart = strpos($string, $startString);

        if ($indexBbcStart !== false) {

            $indexBbcFinish = strpos($string, $finishString, $indexBbcStart);
            $subSring = substr($string, $indexBbcStart, $indexBbcFinish + strlen($finishString));

            $re = '/\d*/m';
            $str = $subSring;
            $matches = [];

            preg_match_all($re, $str, $matches, PREG_PATTERN_ORDER, 0);

            $newArray = array_diff($matches[0], array(
                ''
            ));
            sort($newArray);

            $userId = $newArray[0];

            $remoteUserId = self::getRemoteUserId($userId);

            /**
             * get user Name
             */
            $arRemoteUser = self::getUserDatabyId($remoteUserId);

            $arRemoteUser = \Bitrix\Main\Web\Json::decode($arRemoteUser);

            /**
             * string before user
             */
            $stringBefore = substr($string, 0, $indexBbcStart);

            /**
             * string after user
             */
            $stringAfter = substr($string, $indexBbcFinish + strlen($finishString));

            $stringAfter = self::checkText($stringAfter);
            
            $author = '';
            
            if($remoteUserId == self::$TECH_USER || $remoteUserId == 1){
                
                $dbUser = \CUser::GetByID($userId);
                $arUser = $dbUser->Fetch();
                
                $author .= '('; 
                $author .= $arUser['NAME'];
                $author .= ' ';
                $author .= $arUser['LAST_NAME'];
                $author .= ')';
            }

            $strResult = $stringBefore . ' ' . $startString . $remoteUserId . ']' . $arRemoteUser['result'][0]['NAME'] . ' ' . $arRemoteUser['result'][0]['LAST_NAME'] . $finishString . $author . ' ' . $stringAfter;

            return $strResult;
        } else {

            return $string;
        }
    }

    /**
     * *************
     *
     * @param int $remoteUserId
     * @return string[]|NULL[]|mixed|string
     */
    protected function getUserDatabyId(int $remoteUserId)
    {
        $arParamsUser = [];
        $arResultUser = [];

        $arParamsUser['url'] = self::$restUrl . 'rest/' . self::$hookUserGetCode . 'user.get.json';
        $arParamsUser['followlocation'] = true;
        $arParamsUser['ssl_ignore'] = true;

        $arParamsUser['params']['ID'] = intval($remoteUserId);

        $arResultUser = \Medias\Main\Integration\Rest::callCurl($arParamsUser);

        return $arResultUser;
    }

    /**
     * *
     *
     * @param string $string
     * @param int $userId
     * @return string
     */
    public function changeFileBbc(string $string, string $oldArrId, string $newAttId)
    {
        \Bitrix\Main\Loader::includeModule('disk');
        // $string = 'string before file [DISK FILE ID=n51] string after file [DISK FILE ID=n55]';
        $strResult = '';

        $startString = '[DISK FILE ID=' . $oldArrId;

        $indexBbcStart = strpos($string, $startString);

        $finishString = ']';

        /**
         * *
         * get first file
         */
        if ($indexBbcStart !== false) {

            $indexBbcFinish = strpos($string, $finishString, $indexBbcStart);
            $subSring = substr($string, $indexBbcStart, $indexBbcFinish + strlen($finishString));

            $re = '/\d*/m';
            $str = $subSring;
            $matches = [];

            preg_match_all($re, $str, $matches, PREG_PATTERN_ORDER, 0);

            $newArray = array_diff($matches[0], array(
                ''
            ));
            sort($newArray);

            $fileOnRemoteDiskId = $newAttId;

            $startString = '[DISK FILE ID=';

            $stringBefore = substr($string, 0, $indexBbcStart);

            $stringAfter = substr($string, $indexBbcFinish + strlen($finishString));

            $stringAfter = self::changeFileBbc($stringAfter, $oldArrId, $newAttId);

            $strResult = $stringBefore . ' ' . $startString . $fileOnRemoteDiskId . $finishString . ' ' . $stringAfter;

            return $strResult;
        } else {

            return $string;
        }
    }

    /**
     * *
     *
     * @param int $fileId
     * @return string|boolean
     */
    protected function getDiskFileById(int $fileId)
    {
        \Bitrix\Main\Loader::includeModule('disk');

        $resObjects = \Bitrix\Disk\Internals\ObjectTable::getList([
            'select' => [
                'NAME',
                'FILE_ID',
                'ID'
            ],
            'filter' => [
                '=ID' => $fileId
            ]
        ]);

        if ($arObject = $resObjects->fetch()) {

            $arObject['PATH'] = \CFile::GetPath($arObject['FILE_ID']);
            $arObject['FULL_PATH'] = 'https://' . $_SERVER['SERVER_NAME'] . $arObject['PATH'];

            return $arObject;
        } else {
            return false;
        }
    }

    /**
     * *
     *
     * @param array $arFile
     *            [NAME] => kwitka.txt
     *            [FILE_ID] => 102
     *            [ID] => 51
     *            [PATH] => /upload/disk/331/3311e1bf43e1bb76cf37d4d0b4292a14
     *            [FULL_PATH] => https://ms.medias.com.ua/upload/disk/331/3311e1bf43e1bb76cf37d4d0b4292a14
     */
    protected function getRemoteFile(array $arFile, int $userId)
    {
        $arFields = [];

        $remoteUserId = self::getRemoteUserId($userId);

        $arFields['file'] = $arFile;
        $arFields['userId'] = $remoteUserId;

        $arParams['url'] = self::$restUrl . 'rest/' . self::$restFileSendMedias . 'medias.main.uploadfile.json';
        $arParams['followlocation'] = true;
        $arParams['ssl_ignore'] = true;

        $arParams['params'] = $arFields;

        $arRemoteFile = \Medias\Main\Integration\Rest::callCurl($arParams);

        $arRemoteFile = \Bitrix\Main\Web\Json::decode($arRemoteFile);

        if (is_array($arRemoteFile['result']))
            $remoteId = $arRemoteFile['result']['id'];
        else
            $remoteId = $arRemoteFile['result'];

        self::addFileToRegister(intval($arFile['ID']), $remoteId);

        return $arRemoteFile;
    }

    /**
     * *
     *
     * @param int $localFileId
     * @param int $remoteFileId
     * @return boolean
     */
    protected function addFileToRegister(int $localFileId, int $remoteFileId)
    {
        $arRegister = [];

        $arRegister['UF_LOCAL_FILE_ID'] = $localFileId;
        $arRegister['UF_REMOTE_FILE_ID'] = $remoteFileId;

        $obList = new \Medias\Main\General\Hl('m_task_file_register');

        $resAdd = $obList->addElementToHl($arRegister, [
            'UF_LOCAL_FILE_ID' => $localFileId
        ]);

        return $resAdd;
    }
}
?>