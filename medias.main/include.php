<?
\Bitrix\Main\Loader::registerAutoLoadClasses("medias.main", array(
    // compability classes
    "Medias\Main\General\Handler" => "lib/general/Handler.php",
    "Medias\Main\General\Hl" => "lib/general/Hl.php",
    "Medias\Main\General\GlobalMenu" => "lib/general/GlobalMenu.php",
    "Medias\Main\Integration\Podorognik" => "lib/integration/Podorognik.php",
    "Medias\Main\Integration\Rest" => "lib/integration/Rest.php",
    "Medias\Main\Integration\Api" => "lib/integration/Api.php",
    
/**
 * ***компоненти - *****
 */
));
/* 
\Bitrix\Main\EventManager::addEventHandler("main", "OnBuildGlobalMenu", array(
    "\\Medias\\Main\\General\\GlobalMenu",
    "OnBuildGlobalMenu"
));  */
$obEventManager = \Bitrix\Main\EventManager::getInstance();

$obEventManager->addEventHandler("main", "OnBuildGlobalMenu", array(
    "\\Medias\\Main\\General\\GlobalMenu",
    "OnBuildGlobalMenu"
));  
/**
 * *custom menu in admin part
 */