<?
/**
 * Media Service, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 *
 */

$arModuleVersion = array(
	"VERSION" => "1.6.00",
	"VERSION_DATE" => "2021-12-14 00:00:00"
);
?>