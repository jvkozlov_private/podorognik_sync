<?
/**
 * Media Service, LLC
 *
 * @author Yurii Kozlov <y.kozlov08@gmail.com>
 */
if(!check_bitrix_sessid()) return;?>
<?
	echo CAdminMessage::ShowNote(GetMessage("MODULE_MEDIAS_MAIN_UNINSTALLED"));
?>